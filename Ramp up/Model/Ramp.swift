//
//  Ramp.swift
//  Ramp up
//
//  Created by Olimjon Turgunov on 09.07.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import Foundation
import SceneKit

class Ramp {
    class func getRamp(forName name: String) -> SCNNode {
        switch name {
        case "pipe": return Ramp.getPipe()
        case "pyramid": return Ramp.getPyramid()
        case "quarter": return Ramp.getQaurter()
        default: return Ramp.getPipe()
        }
    }
    
    class func getPipe() -> SCNNode {
        let obj = SCNScene(named: "art.scnassets/pipe.scn")!
        let node = obj.rootNode.childNode(withName: "pipe", recursively: true)!
        node.scale = SCNVector3Make(0.0012, 0.0012, 0.0012)
        node.position = SCNVector3Make(-1, 0.1, -1)
        return node
    }
    
    class func getPyramid() -> SCNNode {
        let obj = SCNScene(named: "art.scnassets/pyramid.scn")!
        let node = obj.rootNode.childNode(withName: "pyramid", recursively: true)!
        node.scale = SCNVector3Make(0.004, 0.0040, 0.0040)
        node.position = SCNVector3Make(-1, -0.50, -1)
        return node
    }
    
    class func getQaurter() -> SCNNode {
        let obj = SCNScene(named: "art.scnassets/quarter.scn")!
        let node = obj.rootNode.childNode(withName: "quarter", recursively: true)!
        node.scale = SCNVector3Make(0.003, 0.003, 0.003)
        node.position = SCNVector3Make(-1, -1.2, -1)
        return node
    }
    
    class func startRotation(_ node: SCNNode) {
        let rotate = SCNAction.repeatForever(SCNAction.rotateBy(x: 0, y: CGFloat(0.01 * Double.pi), z: 0, duration: 0.1))
        node.runAction(rotate)
    }
}
